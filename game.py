import pygame as pg
from settings import *
import sequence_generator as sg
import random
from sprites import Button, SudokuField


class Sudoku:

    def __init__(self):
        pg.init()
        self.screen = pg.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
        self.clock = pg.time.Clock()
        self.running = False
        self.sequence = []

        self._main_lines_thickness = 3
        self._lesser_lines_thickness = 1

        self.reveal_possibility = REVEAL_PERCENTAGE
        self.won = False

        self.selected_field = None

        # sprites 
        self.number_buttons = []
        self.fields = []
        self.attempts_remaining = 3
        self.peek_count = 3
        self.autocomplete_button = None
        self.peek_button = None
        self._init_new_game()

    def _init_new_game(self):
        self.won = False
        self.number_buttons.clear()
        self.fields.clear()
        self.sequence = sg.generate_sequence()
        self._init_number_buttons()
        self._init_fields()
        self._init_help_buttons()
        self.attempts_remaining = 3
        self.peek_count = 3
        self.selected_field = None

    def run(self):
        self.running = True
        while self.running:
            self._set_fps()
            self._update_screen()
            self._handle_events()
        pg.quit()

    def _init_help_buttons(self):
        def peek_command():
            if self.peek_count > 0 and self.selected_field is not None and self.selected_field.hidden:
                self.selected_field.hidden = False
                self.peek_count -= 1
        def autocomplete_button():
            pass
        self.peek_button = Button("Peek", SCREEN_WIDTH - BUTTON_SIZE, SECTOR2_Y_POSITION, BUTTON_SIZE * 2, BUTTON_SIZE, lambda: peek_command())
        self.autocomplete_button = Button("Auto", SCREEN_WIDTH - BUTTON_SIZE * 3, SECTOR2_Y_POSITION, BUTTON_SIZE * 2, BUTTON_SIZE, lambda: autocomplete_button())

    def _draw_help_buttons(self):
        self.peek_button.draw(self.screen)
        self.autocomplete_button.draw(self.screen)

    def _draw_game_over(self):
        self.screen.fill(BLACK)
        self._draw_text(FONT, FONT_SIZE * 2, "GAME OVER", SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, WHITE)

    def _draw_game_won(self):
        self.screen.fill(BLACK)
        self._draw_text(FONT, FONT_SIZE * 2, "YOU WIN", SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, WHITE)

    def _draw_attempts_remaining(self):
        for i in range(self.attempts_remaining):
            pg.draw.circle(self.screen, BLACK, (ATTEMPTS_REMAINING_SIZE + ATTEMPTS_REMAINING_SIZE + ATTEMPTS_REMAINING_SIZE * i * 3, SECTOR2_Y_POSITION), ATTEMPTS_REMAINING_SIZE)

    def _draw_peek_count(self):
        for i in range(self.peek_count):
            pg.draw.circle(self.screen, BLACK, (ATTEMPTS_REMAINING_SIZE + ATTEMPTS_REMAINING_SIZE + ATTEMPTS_REMAINING_SIZE * i * 3, SECTOR2_Y_POSITION + ATTEMPTS_REMAINING_SIZE * 3), ATTEMPTS_REMAINING_SIZE, 2)

    def _handle_events(self):
        for event in pg.event.get():
            if event.type == pg.QUIT:
                self.running = False
            if self.attempts_remaining > 0 and not self.won:
                if event.type == pg.MOUSEMOTION:
                    self._check_button_hover(event.pos)
                if event.type == pg.MOUSEBUTTONDOWN:
                    self._highlight_fields(event.pos)
                    self._handle_button_click(event.pos)
                    if self._all_fields_revealed():
                        self.won = True
            else:
                if event.type == pg.KEYDOWN:
                    self._init_new_game()

    def _set_fps(self):
        self.clock.tick(FPS)

    def _update_screen(self):
        self.screen.fill(WHITE)
        self._draw_fields()
        self._draw_field_lines()
        self._draw_number_buttons()
        self._draw_attempts_remaining()
        self._draw_peek_count()
        self._draw_help_buttons()
        if self.attempts_remaining <= 0:
            self._draw_game_over()
        if self.won:
            self._draw_game_won()
        pg.display.update()

    def _draw_field_lines(self):

        # main vertical lines
        pg.draw.line(self.screen, BLACK, (BOARD_SIZE / 3, 0), (BOARD_SIZE / 3, BOARD_SIZE), self._main_lines_thickness)
        pg.draw.line(self.screen, BLACK, (BOARD_SIZE / 3 * 2, 0), (BOARD_SIZE / 3 * 2, BOARD_SIZE), self._main_lines_thickness)

        # main horizontal lines
        pg.draw.line(self.screen, BLACK, (0, BOARD_SIZE / 3), (BOARD_SIZE, BOARD_SIZE / 3), self._main_lines_thickness)
        pg.draw.line(self.screen, BLACK, (0, BOARD_SIZE / 3 * 2), (BOARD_SIZE, BOARD_SIZE / 3 * 2), self._main_lines_thickness)
        pg.draw.line(self.screen, BLACK, (0, BOARD_SIZE), (BOARD_SIZE, BOARD_SIZE), self._main_lines_thickness)

        # lesser vertical lines
        pg.draw.line(self.screen, BLACK, (BOARD_SIZE / 9, 0), (BOARD_SIZE / 9, BOARD_SIZE), self._lesser_lines_thickness)
        pg.draw.line(self.screen, BLACK, (BOARD_SIZE / 9 * 2, 0), (BOARD_SIZE / 9 * 2, BOARD_SIZE), self._lesser_lines_thickness)
        pg.draw.line(self.screen, BLACK, (BOARD_SIZE / 9 * 4, 0), (BOARD_SIZE / 9 * 4, BOARD_SIZE), self._lesser_lines_thickness)
        pg.draw.line(self.screen, BLACK, (BOARD_SIZE / 9 * 5, 0), (BOARD_SIZE / 9 * 5, BOARD_SIZE), self._lesser_lines_thickness)
        pg.draw.line(self.screen, BLACK, (BOARD_SIZE / 9 * 7, 0), (BOARD_SIZE / 9 * 7, BOARD_SIZE), self._lesser_lines_thickness)
        pg.draw.line(self.screen, BLACK, (BOARD_SIZE / 9 * 8, 0), (BOARD_SIZE / 9 * 8, BOARD_SIZE), self._lesser_lines_thickness)

        # lesser horizontal lines
        pg.draw.line(self.screen, BLACK, (0, BOARD_SIZE / 9), (BOARD_SIZE, BOARD_SIZE / 9), self._lesser_lines_thickness)
        pg.draw.line(self.screen, BLACK, (0, BOARD_SIZE / 9 * 2), (BOARD_SIZE, BOARD_SIZE / 9 * 2), self._lesser_lines_thickness)
        pg.draw.line(self.screen, BLACK, (0, BOARD_SIZE / 9 * 4), (BOARD_SIZE, BOARD_SIZE / 9 * 4), self._lesser_lines_thickness)
        pg.draw.line(self.screen, BLACK, (0, BOARD_SIZE / 9 * 5), (BOARD_SIZE, BOARD_SIZE / 9 * 5), self._lesser_lines_thickness)
        pg.draw.line(self.screen, BLACK, (0, BOARD_SIZE / 9 * 7), (BOARD_SIZE, BOARD_SIZE / 9 * 7), self._lesser_lines_thickness)
        pg.draw.line(self.screen, BLACK, (0, BOARD_SIZE / 9 * 8), (BOARD_SIZE, BOARD_SIZE / 9 * 8), self._lesser_lines_thickness)

    def _init_fields(self):
        for i in range(len(FIELDS)):
            for j in range(len(FIELDS[i])):
                # TODO: Refactor
                sector = 1
                if i < 3 and j < 3:
                    sector = 1
                elif i < 3 and j < 6:
                    sector = 2
                elif i < 3 and j < 9:
                    sector = 3
                elif i < 6 and j < 3:
                    sector = 4
                elif i < 6 and j < 6:
                    sector = 5
                elif i < 6 and j < 9:
                    sector = 6
                elif i < 9 and j < 3:
                    sector = 7
                elif i < 9 and j < 6:
                    sector = 8
                elif i < 9 and j < 9:
                    sector = 9

                content = str(self.sequence[i][j])
                field = SudokuField(content, FIELDS[i][j][0], FIELDS[i][j][1], FIELD_SIZE, FIELD_SIZE, sector, (i, j), self.reveal_possibility < random.random())
                self.fields.append(field)

    def _draw_fields(self):
        for field in self.fields:
            field.draw(self.screen)

    def _draw_text(self, font, size, text, x, y, color):
        sysfont = pg.font.SysFont(font, size)
        text = sysfont.render(text, 1, color)
        text_rect = text.get_rect()
        text_rect.center = (x, y)
        self.screen.blit(text, text_rect)

    def _init_number_buttons(self):
        def command(number):
            if self.selected_field is None or not self.selected_field.hidden:
                return
            
            if int(self.selected_field.content) == number:
                self.selected_field.hidden = False
            else:
                self.attempts_remaining -= 1

        for i in range(1, 10):
            number_button = Button(i, BUTTON_SIZE / 2 + BUTTON_SIZE * (i - 1), SECTOR1_Y_POSITION, BUTTON_SIZE, BUTTON_SIZE, lambda x=i: command(x))
            self.number_buttons.append(number_button)

    def _draw_number_buttons(self):
        for number_button in self.number_buttons:
            number_button.draw(self.screen)

    def _button_pointer_collided(self, button, pointer_pos):
        if button.rect.left <= pointer_pos[0] <= button.rect.right:
            if button.rect.top <= pointer_pos[1] <= button.rect.bottom:
                return True
        return False

    def _check_button_hover(self, pointer_pos):
        self._check_number_buttons_hover(pointer_pos)
        self._check_help_buttons_hover(pointer_pos)
    
    def _check_help_buttons_hover(self, pointer_pos):
        if self._button_pointer_collided(self.peek_button, pointer_pos):
            self.peek_button.hovering = True
        else:
            self.peek_button.hovering = False
        if self._button_pointer_collided(self.autocomplete_button, pointer_pos):
            self.autocomplete_button.hovering = True
        else:
            self.autocomplete_button.hovering = False

    def _check_number_buttons_hover(self, pointer_pos):
        for number_button in self.number_buttons:
            if self._button_pointer_collided(number_button, pointer_pos):
                number_button.hovering = True
                continue
            number_button.hovering = False
            
    def _highlight_fields(self, pointer_pos):
        if pointer_pos[0] > BOARD_SIZE or pointer_pos[1] > BOARD_SIZE:
            return

        highlighted_field = None
        for field in self.fields:
            field.highlighted = False
            if field.rect.left <= pointer_pos[0] <= field.rect.right:
                if field.rect.top <= pointer_pos[1] <= field.rect.bottom:
                    highlighted_field = field
            
        if highlighted_field is not None:
            if highlighted_field.hidden:
                highlighted_field.highlighted = True
                self.selected_field = highlighted_field
            else:
                for field in self.fields:
                    if field.matrix_pos[0] == highlighted_field.matrix_pos[0] or field.matrix_pos[1] == highlighted_field.matrix_pos[1] or field.sector == highlighted_field.sector:
                        field.highlighted = True
                    if field.content == highlighted_field.content and not field.hidden:
                        field.highlighted = True
    
    def _handle_button_click(self, pointer_pos):
        for number_button in self.number_buttons:
            if self._button_pointer_collided(number_button, pointer_pos):
                number_button.command()
                break
        
        if self._button_pointer_collided(self.peek_button, pointer_pos):
            self.peek_button.command()
        if self._button_pointer_collided(self.autocomplete_button, pointer_pos):
            self.autocomplete_button.command()

    def _all_fields_revealed(self):
        for field in self.fields:
            if field.hidden:
                return False
        
        return True

            
                



