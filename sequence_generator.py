import random
import copy


def generate_sequence():
    # sequence = [list(range(1, 10)) for _ in range(9)] 
    random_array = sorted(range(1, 10), key=lambda x: random.random())
    sequence = [copy.deepcopy(random_array) for _ in range(9)]

    # creating a 9x9 matrix that has the correct order of numbers for individual 3x3 matrices
    for i in range(len(sequence)):
        if i % 3 == 1:
            sequence[i][:3], sequence[i][3:6], sequence[i][6:] = sequence[i][6:], sequence[i][:3], sequence[i][3:6]
        elif i % 3 == 2:
            sequence[i][:3], sequence[i][3:6], sequence[i][6:] = sequence[i][3:6], sequence[i][6:], sequence[i][:3]

    # shuffling the the rows of the 3x3 matrices to create a fully correct order of numbers (correct when looking at rows and columns)
    for i in range(3, len(sequence)):
        if i < 6:
            sequence[i][0], sequence[i][1], sequence[i][2] = sequence[i][2], sequence[i][0], sequence[i][1]
            sequence[i][3], sequence[i][4], sequence[i][5] = sequence[i][5], sequence[i][3], sequence[i][4]
            sequence[i][6], sequence[i][7], sequence[i][8] = sequence[i][8], sequence[i][6], sequence[i][7]
        else:
            sequence[i][0], sequence[i][1], sequence[i][2] = sequence[i][1], sequence[i][2], sequence[i][0]
            sequence[i][3], sequence[i][4], sequence[i][5] = sequence[i][4], sequence[i][5], sequence[i][3]
            sequence[i][6], sequence[i][7], sequence[i][8] = sequence[i][7], sequence[i][8], sequence[i][6]

    # making the order more random
    # in order not to break the correct order we generated, we generate three list of indexes
    # the indexes lists will contain three numbers (indexes)
    # the first will contain [0, 1, 2], second [3, 4, 5] and the third [6, 7, 8]
    # first three numbers (same goes for the second three numbers and third) must be shuffled with the same indexes so we don't break the correct order
    first_segment_indexes = sorted(range(3), key=lambda x: random.random())
    second_segment_indexes = sorted(range(3, 6), key=lambda x: random.random())
    third_segment_indexes = sorted(range(6, 9), key=lambda x: random.random())
    for i in range(len(sequence)):
        sequence[i][0], sequence[i][1], sequence[i][2] = sequence[i][first_segment_indexes[0]], sequence[i][first_segment_indexes[1]], sequence[i][first_segment_indexes[2]]
        sequence[i][3], sequence[i][4], sequence[i][5] = sequence[i][second_segment_indexes[0]], sequence[i][second_segment_indexes[1]], sequence[i][second_segment_indexes[2]]
        sequence[i][6], sequence[i][7], sequence[i][8] = sequence[i][third_segment_indexes[0]], sequence[i][third_segment_indexes[1]], sequence[i][third_segment_indexes[2]]

    return sequence
