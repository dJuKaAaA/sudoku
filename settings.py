# colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
GRAY = (190, 190, 190)
LIGHT_GRAY = (220, 220, 220)

# game
FONT = None
FONT_SIZE = 32
FPS = 60
SCREEN_WIDTH = 600
SCREEN_HEIGHT = SCREEN_WIDTH * 4 / 3 
BOARD_SIZE = SCREEN_WIDTH
FIELD_SIZE = BOARD_SIZE / 9
BUTTON_SIZE = SCREEN_WIDTH / 9
SECTOR1_Y_POSITION = SCREEN_WIDTH + BUTTON_SIZE
SECTOR2_Y_POSITION = SECTOR1_Y_POSITION + BUTTON_SIZE * 1.1
ATTEMPTS_REMAINING_SIZE = 10 

# the percentage of the fields whoose content will be visible
REVEAL_PERCENTAGE = 0.5

# coordinates of field values
FIELDS = [[] for _ in range(9)]
for i in range(9):
    for j in range(9):
        FIELDS[i].append((BOARD_SIZE / 18 + BOARD_SIZE / 9 * i, BOARD_SIZE / 18 + BOARD_SIZE / 9 * j))


