import pygame as pg
from settings import *


pg.init()
font = pg.font.SysFont(FONT, FONT_SIZE)


class Button:

    def __init__(self, content, x, y, width, height, command):
        self.rect = pg.Rect(x, y, width, height)
        self.rect.center = (x, y)
        self.content = content
        self.hovering = False
        self.command = command

    def draw(self, screen):
        if self.hovering:
            pg.draw.rect(screen, GRAY, self.rect)
        pg.draw.rect(screen, BLACK, self.rect, 2)
        text = font.render(str(self.content), 1, BLACK)
        text_rect = text.get_rect()
        text_rect.center = self.rect.center
        screen.blit(text, text_rect)


class SudokuField:

    def __init__(self, content, x, y, width, height, sector, matrix_pos, hidden):
        self.rect = pg.Rect(x, y, width, height)
        self.rect.center = (x, y)
        self.content = content
        self.highlighted = False
        self.sector = sector
        self.matrix_pos = matrix_pos
        self.hidden = hidden

    def draw(self, screen):
        if self.highlighted:
            pg.draw.rect(screen, GRAY, self.rect)
        if not self.hidden:
            text = font.render(str(self.content), 1, BLACK)
            text_rect = text.get_rect()
            text_rect.center = self.rect.center
            screen.blit(text, text_rect)
    